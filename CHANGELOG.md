# Changelog
All notable changes to PyINI will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2.1 - 2022/12/27
- Transfer over to pyproject.toml, fixes #1.
- Fix some type hints
- Add SPDX license identifier

## 2.0 - 2021/12/22
- Entirely different approach

## 1.0 - 2022/4/25
- Initial release, forked from [https://github.com/PolyEdge/PyINI](https://github.com/PolyEdge/PyINI)
